function varargout = CollegeFootballGUI(varargin)
%COLLEGEFOOTBALLGUI MATLAB code file for CollegeFootballGUI.fig
%      COLLEGEFOOTBALLGUI, by itself, creates a new COLLEGEFOOTBALLGUI or raises the existing
%      singleton*.
%
%      H = COLLEGEFOOTBALLGUI returns the handle to a new COLLEGEFOOTBALLGUI or the handle to
%      the existing singleton*.
%
%      COLLEGEFOOTBALLGUI('Property','Value',...) creates a new COLLEGEFOOTBALLGUI using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to CollegeFootballGUI_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      COLLEGEFOOTBALLGUI('CALLBACK') and COLLEGEFOOTBALLGUI('CALLBACK',hObject,...) call the
%      local function named CALLBACK in COLLEGEFOOTBALLGUI.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help CollegeFootballGUI

% Last Modified by GUIDE v2.5 11-Dec-2016 17:21:15

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @CollegeFootballGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @CollegeFootballGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [], ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
   gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before CollegeFootballGUI is made visible.
function CollegeFootballGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for CollegeFootballGUI
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% create an axes that spans the whole gui

ah = axes('unit', 'normalized', 'position', [0 0 1 1]);

% import the background image and show it on the axes

bg = imread('BryantDenny.jpg'); imagesc(bg);

% prevent plotting over the background and turn the axis off

set(ah,'handlevisibility','off','visible','off')

% making sure the background is behind all the other uicontrols
uistack(ah, 'bottom');

% UIWAIT makes CollegeFootballGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = CollegeFootballGUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%axes(handles.coach);
%contents = get(handles.popupmenu1,'String');
str = get(hObject, 'String');
contents = get(hObject, 'Value')

switch str{contents}
    case 'Alabama'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('saban.jpg');
        imshow(p);
        
        axes(handles.mascot);
        p = imread('BamaMascot.jpg');   %handle mascot picture
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Nick Saban';
        championships = '16';
        record = '118�18';
        
        
    case 'Arkansas'
        %change pictures for coach and mascot
        axes(handles.currentCoach);
        p = imread('BretBielema.jpg');  %handle coach picture
        imshow(p);
        
        axes(handles.mascot);
        p = imread('Razorback.jpg');    %handle mascot picture
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Bret Bielema';
        championships = '1';
        record = '25�25';
        
    case 'Auburn'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('GusMalzahn.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('Aubie.png');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Gus Malzahn';
        championships = '2';
        record = '35�17';
        
    case 'Florida'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('JimMcElwain.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('FloridaGator.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Jim McElwain';
        championships = '3';
        record = '18�8';
        
    case 'Georgia'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('KirbySmart.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('GeorgiaBulldog.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Kirby Smart';
        championships = '5';
        record = '7�5';
        
    case 'Kentucky'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('MarkStoops.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('KentuckyWildcat.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Mark Stoops';
        championships = '1';
        record = '19-29';
        
    case 'LSU'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('EdOrgeron.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('LSUTiger.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Ed Orgeron';
        championships = '3';
        record = '5-2';
        
    case 'Mississippi State'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('DanMullen.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('MississippiStateBulldog.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Dan Mullen';
        championships = '0';
        record = '60-42';
        
    case 'Missouri'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('BarryOdom.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('MissouriTiger.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Barry Odom';
        championships = '0';
        record = '4-8';
        
    case 'Ole Miss'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('HughFreeze.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('OleMissBlackBear.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Hugh Freeze';
        championships = '3';
        record = '39-25';
        
    case 'South Carolina'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('WillMuschamp.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('SouthCarolinaCocky.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Will Muschamp';
        championships = '0';
        record = '6-6';
        
    case 'Tennessee'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('ButchJones.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('TennesseeVols.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Butch Jones';
        championships = '6';
        record = '29-21';
        
    case 'Texas A&M'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('KevinSumlin.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('TexasA&MAggie.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Kevin Sumlin';
        championships = '3';
        record = '44-20';
        
    case 'Vanderbilt'
        %change pictures for coach and mascot
        axes(handles.currentCoach);     %handle coach picture
        p = imread('DerekMason.jpg');
        imshow(p);
        
        axes(handles.mascot);           %handle mascot picture
        p = imread('VanderbiltCommodore.jpg');
        imshow(p);
        
        %change coachName, championship and record text variables
        coachName = 'Derek Mason';
        championships = '0';
        record = '13-23';
end

set(handles.coach,'String',coachName);
set(handles.championships,'String',championships);
set(handles.record,'String',record);

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1




% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
